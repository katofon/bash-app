require 'rails_helper'

RSpec.describe Article, type: :model do
  context "validation tests" do

    it "ensures body is present" do
      article = Article.new(created_at: Time.now)
      expect(article.valid?).to eq(false)

      article = Article.new(created_at: Time.now, body: "")
      expect(article.valid?).to eq(false)
    end

    it "ensures created_at is present" do
      article = Article.new(body: "Test body")
      expect(article.valid?).to eq(false)
    end

    it "ensures rating is 0 by default" do
      article = Article.create(body: "Test body", created_at: Time.now)
      expect(article.rating).to eq(Article::DEFAULT_RATING)
    end
  end

end
