class ArticlesController < ApplicationController
  SESSION_VOTED_ARTICLES = :voted_articles

  def index
    sort_by = "created_at"
    sort_by = "rating" if params[:sort_by] == "rating"
    @articles = Article.all.sort{|f, s| s[sort_by] <=> f[sort_by]}
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)
    @article["created_at"] = Time.now

    respond_to do |format|
      if @article.save
        format.html { redirect_to root_path}
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def dislike
    rate_for_article(-1, params[:id])
  end

  def like
    rate_for_article(1, params[:id])
  end

  def rate_for_article modifier, article_id
    voted_articles = session[SESSION_VOTED_ARTICLES] || []
    return if voted_articles.include?(article_id)

    @article = Article.find(article_id)
    return if @article.nil?

    @article.rate! modifier
    voted_articles << article_id
    session[SESSION_VOTED_ARTICLES] = voted_articles

    render js: "$('##{article_id}').html('#{@article.rating}')"
  end

  private

  def article_params
    params.require(:article).permit(:body)
  end

end
