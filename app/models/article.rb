class Article < ApplicationRecord
  DEFAULT_RATING = 0

  validates_presence_of :created_at
  validates :body, presence: true, allow_blank: false

  def rate! modifier
    update(rating: self.rating + modifier)
  end

end
